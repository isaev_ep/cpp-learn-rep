
#include <algorithm>
#include <iostream>
#include <ostream>
#include <string>
#include <vector>

// Vehicle

class Vehicle
{
public:
    virtual ~Vehicle()
    {
        std::cout << "Vehicle destructor" << std::endl;
    };
    virtual std::ostream& Print(std::ostream& out) const = 0;
    friend std::ostream& operator << (std::ostream& out, const Vehicle& vehicle);
};

std::ostream& operator<<(std::ostream& out, const Vehicle& vehicle)
{
    return vehicle.Print(out);
}

// WaterVehicle

class WaterVehicle : public Vehicle
{
public:
    float draft;
    WaterVehicle(float draft) : draft(draft) {}
    std::ostream& Print(std::ostream& out) const override;
    ~WaterVehicle() override;
};

WaterVehicle::~WaterVehicle()
{
    std::cout << "WaterVehicle destructor" << std::endl;
};

std::ostream& WaterVehicle::Print(std::ostream& out) const
{
    out << "WaterVehicle:" << std::endl;
    out << "> Draft: " << draft << std::endl;
    return out;
}

// RoadVehicle

class RoadVehicle :public Vehicle
{
public:
    float clearance;
    RoadVehicle(float clearance) : clearance(clearance) {}
    std::ostream& Print(std::ostream& out) const override;
    ~RoadVehicle() override;
};

RoadVehicle::~RoadVehicle()
{
    std::cout << "RoadVehicle destructor" << std::endl;
};

std::ostream& RoadVehicle::Print(std::ostream& out) const
{
    out << "RoadVehicle:" << std::endl;
    out << "> Clearance: " << clearance << std::endl;
    return out;
}

// Wheel

class Wheel
{
public:
    int diameter;
    Wheel(int diameter) : diameter(diameter) {};
};

// Point

class Point
{
public:
    int x;
    int y;
    int z;

    Point(int x, int y, int z) : x(x), y(y), z(z) {};
};

// Engine

class Engine
{
public:
    int power;
    Engine(int power) : power(power) {};
};

// Car

class Car : public RoadVehicle
{
public:
    Engine engine;
    Wheel wheelFR;
    Wheel wheelFL;
    Wheel wheelBR;
    Wheel wheelBL;

    Car(const Engine& engine, const Wheel& wheelFR, const Wheel& wheelFL, const Wheel& wheelBR, const Wheel& wheelBL, float clearance) :
        RoadVehicle(clearance), engine(engine), wheelFR(wheelFR), wheelFL(wheelFL), wheelBR(wheelBR), wheelBL(wheelBL) {}

    std::ostream& Print(std::ostream& out) const override;
    ~Car() override;
};

Car::~Car()
{
    std::cout << "Car destructor" << std::endl;
};

std::ostream& Car::Print(std::ostream& out) const
{
    out << "Car:" << std::endl;
    out << "> Engine: " << engine.power << std::endl;
    out << "> Wheels: " << wheelFR.diameter << ", " << wheelFL.diameter << ", " << wheelBL.diameter << ", " << wheelBR.diameter << std::endl;
    out << "> Ride height: " << clearance << std::endl;
    return out;
}

// Bicycle

class Bicycle : public RoadVehicle
{
public:
    Wheel wheelForward;
    Wheel wheelBackward;

    Bicycle(const Wheel& wheelForward, const Wheel& wheelBackward, float clearance) :
        RoadVehicle(clearance), wheelForward(wheelForward), wheelBackward(wheelBackward) {}

    std::ostream& Print(std::ostream& out) const override;
    ~Bicycle() override;
};

Bicycle::~Bicycle()
{
    std::cout << "Bicycle destructor" << std::endl;
};

std::ostream& Bicycle::Print(std::ostream& out) const
{
    out << "Bicycle:" << std::endl;
    out << "> Wheels: " << wheelForward.diameter << ", " << wheelBackward.diameter << std::endl;
    out << "> Ride height: " << clearance << std::endl;
    return out;
}

// Circle

class Circle : public Vehicle
{
public:
    Point point;
    int diameter;

    Circle(const Point& point, int diameter) : point(point), diameter(diameter) {}

    std::ostream& Print(std::ostream& out) const override;
    ~Circle() override;
};

Circle::~Circle()
{
    std::cout << "Circle destructor" << std::endl;
};

std::ostream& Circle::Print(std::ostream& out) const
{
    out << "Circle:" << std::endl;
    out << "> Point: " << diameter << std::endl;
    out << "> Diameter: " << diameter << std::endl;
    return out;
}

// main

std::string getHighestPower(const std::vector<Vehicle*>& vector)
{
    int  max_power = 0;

    for (const auto vehicle : vector)
    {
        if (const auto car = dynamic_cast<Car*>(vehicle))
        {
            int power = car->engine.power;
            max_power = (power > max_power) ? power : max_power;
        }
    };

    return std::to_string(max_power);
};

int main()
{
    Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);

    std::cout << c << '\n';

    Bicycle t(Wheel(20), Wheel(20), 300);

    std::cout << t << '\n';

    std::vector<Vehicle*> v;

    v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));

    v.push_back(new Circle(Point(1, 2, 3), 7));

    v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));

    v.push_back(new WaterVehicle(5000));

    for (auto vehicle : v)
    {
        std::cout << *(vehicle) << std::endl;
    }

    std::cout << "The highest power is " << getHighestPower(v) << std::endl;

    for (auto vehicle : v)
    {
        delete vehicle;
    }

    return 0;
}
