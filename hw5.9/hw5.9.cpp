#include <iostream>
#include <string>

class Fraction
{
public:
    int numerator;
    int denominator;

    Fraction(int numerator, int denominator) : numerator(numerator), denominator(denominator)
    {
        if (denominator == 0)
        {
            throw std::runtime_error("Denominator cannot be zero");
        }
    };

    friend std::ostream& operator << (std::ostream& out, const Fraction& fraction);
};

std::ostream& operator<<(std::ostream& out, const Fraction& fraction)
{
    out << std::to_string(fraction.numerator) << "/" << std::to_string(fraction.denominator) << std::endl;
    return out;
}

int main(int argc, char* argv[])
{
    int a, b;
    std::cout << "Enter numerator:";
    std::cin >> a;
    std::cout << "Enter denominator:";
    std::cin >> b;

    try
    {
        Fraction fraction(a, b);
        std::cout << fraction;
    }
    catch (std::exception exception)
    {
        std::cout << "Exception: " << exception.what() << std::endl;
    }

    return 0;
}
