#pragma once
#include "Building.h"
#include "Mage.h"
#include "Warrior.h"

class MageTower : Building
{
public:
    MageTower() {};
    Mage* createMage(Outfit* outfit, Weapon* weapon);
};
