#pragma once
#include "Barrack.h"
#include "Forge.h"
#include "MageTower.h"

class Builder;
class Village
{
    friend class Builder;

    Forge* forge = nullptr;
    MageTower* mage_tower = nullptr;
    Barrack* barrack = nullptr;

    void buildForge();
    void buildMageTower();
    void buildBarrack();

public:
    Village() {}

    Forge* getForge();
    MageTower* getMageTower();
    Barrack* getBarrack();
};
