#pragma once
#include "Building.h"
#include "Warrior.h"

class Barrack : Building
{
public:
    Barrack() {};
    Warrior* createWarrior(Outfit* outfit, Weapon* weapon);
};
