#include "Builder.h"

void Builder::buildVillage()
{
    village = new Village();
    village->buildForge();
    village->buildMageTower();
    village->buildBarrack();
}

Village* Builder::getVillage()
{
    return village;
}
