#pragma once
#include "Building.h"
#include "Outfit.h"
#include "Weapon.h"

class Forge : Building
{
public:
    Forge() {};
    Outfit* createArmor();
    Weapon* createStaff();
    Outfit* createRobe();
    Weapon* createSword();
    Weapon* createMace();
};
