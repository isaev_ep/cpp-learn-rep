#pragma once
#include "Item.h"

class Outfit : public Item
{
    unsigned short durability;
public:
    Outfit(std::string name, unsigned short durability);
};
