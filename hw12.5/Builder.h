#pragma once
#include "Village.h"

class Builder
{
    Village* village = nullptr;
public:

    Builder() { }

    void buildVillage();
    Village* getVillage();
};
