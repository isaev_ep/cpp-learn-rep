#include "Unit.h"

#include <algorithm>
#include <iostream>

Unit::Unit(std::string name, unsigned short health, Outfit* outfit, Weapon* weapon)
    : name(name), health(health), outfit(outfit), weapon(weapon)
{
    max_health = health;
}

Unit* Unit::copy()
{
    return new Unit(name, health, outfit, weapon);
}

void Unit::print()
{
    std::cout << "=== " << name << " ===" << std::endl;
    std::cout << "> Health: " << health << "/" << max_health << std::endl;
    std::cout << "> Outfit: " << outfit->getName() << std::endl;
    std::cout << "> Weapon: " << weapon->getName() << std::endl;
    std::cout << std::endl;
}

void Unit::hit(int value)
{
    health = std::clamp<int>(health + value, 0, max_health);
}

Weapon* Unit::changeWeapon(Weapon* new_weapon)
{
    Weapon* old_weapon = weapon;
    weapon = new_weapon;
    return old_weapon;
}
