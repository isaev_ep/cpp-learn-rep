#pragma once
#include <string>

#include "Copyable.h"
#include "Outfit.h"
#include "Weapon.h"

class Unit : public Copyable
{

    std::string name = "Unit";
    unsigned short health = 0;
    unsigned short max_health = 0;

    Outfit* outfit;
    Weapon* weapon;

public:
    explicit Unit(std::string name, unsigned short health, Outfit* outfit, Weapon* weapon);
    Unit* copy() override;
    void print();
    void hit(int value);
    Weapon* changeWeapon(Weapon* new_weapon);
};
