#include "Village.h"

void Village::buildForge()
{
    forge = new Forge();
}

void Village::buildMageTower()
{
    mage_tower = new MageTower();
}

void Village::buildBarrack()
{
    barrack = new Barrack();
}

Forge* Village::getForge()
{
    return forge;
}

MageTower* Village::getMageTower()
{
    return mage_tower;
}

Barrack* Village::getBarrack()
{
    return barrack;
}
