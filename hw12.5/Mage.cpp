#include "Mage.h"

Mage::Mage(Outfit* outfit, Weapon* weapon): Unit("Mage", 80, outfit, weapon) { }

Unit* Mage::createClone(Unit* unit)
{
    return unit->copy();
}

void Mage::heal(Unit* unit, unsigned short value)
{
    unit->hit(value);
}
