#include "Builder.h"
#include "DataBaseConnection.h"
#include "Mage.h"
#include "Village.h"
#include "Warrior.h"

int main(int argc, char* argv[])
{
    // Одиночка
    auto db = DataBaseConnection::getInstance();

    // Строитель
    Builder builder;
    builder.buildVillage();
    Village* village = builder.getVillage();

    // Абстрактная фабрика
    Outfit* robe = village->getForge()->createRobe();
    Weapon* staff = village->getForge()->createStaff();
    Outfit* armor = village->getForge()->createArmor();
    Weapon* sword = village->getForge()->createSword();

    Mage* mage = village->getMageTower()->createMage(robe, staff);
    Warrior* warrior = village->getBarrack()->createWarrior(armor, sword);
    // Прототип
    Warrior* cloned_warrior = static_cast<Warrior*>(mage->createClone(warrior));

    // Test
    warrior->hit(-15);
    cloned_warrior->hit(-20);
    mage->heal(warrior, 10);

    Weapon* mace = village->getForge()->createMace();
    Weapon* cloned_sword = cloned_warrior->changeWeapon(mace);

    mage->print();
    warrior->print();
    cloned_warrior->print();

    return 0;
}
