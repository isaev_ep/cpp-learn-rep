#pragma once
#include "Item.h"

class Weapon: public Item
{
    unsigned short damage;
public:
    Weapon(std::string name, unsigned short damage);
};
