#include "EarthDecorator.h"

#include <iostream>

void EarthDecorator::Execute()
{
    BaseMagicDecorator::Execute();

    std::cout << "Earth Spell" << std::endl;
}
