#include "BaseMagicDecorator.h"

BaseMagicDecorator::BaseMagicDecorator(MagicSpell* magic_spell)
{
    previous_magic_spell = magic_spell;
}

BaseMagicDecorator::~BaseMagicDecorator()
{
    delete previous_magic_spell;
}

void BaseMagicDecorator::Execute()
{
    previous_magic_spell->Execute();
}
