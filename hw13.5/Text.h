#pragma once
#include <iostream>

#include "UIElement.h"

class Text : public UIElement
{
public:
    void Draw() override
    {
        std::cout << "Text UIElement" << std::endl;
    }
};
