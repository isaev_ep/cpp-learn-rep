#pragma once
#include "BaseMagicDecorator.h"

class FireDecorator : public BaseMagicDecorator
{
public:
    explicit FireDecorator(MagicSpell* magic_spell)
        : BaseMagicDecorator(magic_spell)
    {
    }

    void Execute() override;
};
