#pragma once
#include <iostream>

#include "UIElement.h"

class Button : public UIElement
{
public:
    void Draw() override
    {
        std::cout << "Button UIElement" << std::endl;
    }
};
