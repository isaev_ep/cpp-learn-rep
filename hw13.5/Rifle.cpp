#include "Rifle.h"

#include <iostream>

void Rifle::Fire()
{
    std::cout << "Rifle::Fire()" << std::endl;
}

void Rifle::Reload()
{
    std::cout << "Rifle::Reload()" << std::endl;
}


Rifle::~Rifle()
{
}
