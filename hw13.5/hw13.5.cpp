
#include <string>

#include "Button.h"
#include "Data.h"
#include "DataBase.h"
#include "EarthDecorator.h"
#include "FireDecorator.h"
#include "MagicSpell.h"
#include "ProgressBar.h"
#include "Rifle.h"
#include "Text.h"
#include "UIContainer.h"
#include "UIWidget.h"
#include "Unit.h"

int main(int argc, char* argv[])
{
    std::setlocale(LC_ALL, "en_US.UTF-8");
    
    // Адаптер class UIElement { void Draw(); }
    std::cout << "### Адаптер ###" << std::endl;
    
    ProgressBar progress_bar;
    progress_bar.Draw();
    Button button;
    button.Draw();
    Text text;
    text.Draw();

    // Компоновщик UIElement[]
    std::cout << "### Компоновщик ###" << std::endl;
    
    UIContainer container;
    container.Add(&button);
    container.Add(&text);
    container.Add(&progress_bar);
    container.Draw();

    // Мост Unit -> Weapon
    std::cout << "### Мост ###" << std::endl;
    
    Unit unit;
    unit.TakeWeapon(new Rifle());
    unit.Fire();
    unit.Reload();

    // Декоратор MagicSpell -> FireSpell -> EarthSpell
    std::cout << "### Декоратор ###" << std::endl;
    
    auto magic_spell = new MagicSpell();
    magic_spell = new EarthDecorator(magic_spell);
    magic_spell = new FireDecorator(magic_spell);
    magic_spell->Execute();

    // Фасад UIWidget
    std::cout << "### Фасад ###" << std::endl;

    UIWidget widget;
    widget.Build();
    widget.Draw();
    
    // Легковес UnitFactory
    std::cout << "### Легковес ###" << std::endl;
    Model model1, model2;
    Texture texture1, texture2;
    Unit unit1(EUnitType::Mage, &model1, &texture1);
    Unit unit2(EUnitType::Mage, &model1, &texture1);
    Unit unit3(EUnitType::Rogue, &model1, &texture1);
    Unit unit4(EUnitType::Rogue, &model2, &texture2);
    Unit unit5(EUnitType::Rogue, &model1, &texture1);

    // Заместитель DataBase::LazyLoadData()
    std::cout << "### Заместитель ###" << std::endl;
    DataBase db;
    std::string finding_data_type1 = "SomeType1";
    std::string finding_data_type2 = "SomeType2";
    std::string finding_data_type3 = "SomeType1";
    Data* data1 = db.LazyLoadData(finding_data_type1);
    Data* data2 = db.LazyLoadData(finding_data_type2);
    Data* data3 = db.LazyLoadData(finding_data_type3);

    return 0;
}
