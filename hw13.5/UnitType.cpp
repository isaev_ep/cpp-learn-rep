#include "UnitType.h"

UnitType::UnitType(EUnitType type, Model* model, Texture* texture)
    : type(type), model(model), texture(texture)
{
}

bool UnitType::isEqual(EUnitType other_type, const Model* other_model, const Texture* other_texture) const
{
    return type == other_type && model == other_model && texture == other_texture;
}
