#pragma once
#include "UIContainer.h"

class UIWidget
{
    UIContainer container;
public:
    void Build();
    void Draw();
};
