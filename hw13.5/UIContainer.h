#pragma once
#include <vector>

#include "UIElement.h"

class UIContainer : public UIElement
{
    std::vector<UIElement*> childrens;
    
public:
    void Draw() override;
    void Add(UIElement* element);
    void Remove(UIElement* element);
};
