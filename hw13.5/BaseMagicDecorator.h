#pragma once
#include "MagicSpell.h"

class BaseMagicDecorator : public MagicSpell
{
    MagicSpell* previous_magic_spell  = nullptr;

public:
    BaseMagicDecorator(MagicSpell* magic_spell);
    ~BaseMagicDecorator() override;
    void Execute() override;
};
