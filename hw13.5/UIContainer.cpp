#include "UIContainer.h"

void UIContainer::Draw()
{
    for (auto child : childrens)
    {
        child->Draw();
    }
}

void UIContainer::Add(UIElement* element)
{
    childrens.push_back(element);
}

void UIContainer::Remove(UIElement* element)
{
    childrens.erase(std::remove(childrens.begin(), childrens.end(), element), childrens.end());
}
