#include "FireDecorator.h"

#include <iostream>

void FireDecorator::Execute()
{
    BaseMagicDecorator::Execute();

    std::cout << "Fire Spell" << std::endl;
}
