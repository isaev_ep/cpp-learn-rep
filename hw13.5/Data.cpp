#include "Data.h"

Data::Data(std::string &type) : type(type)
{
    
}

bool Data::isEqual(const std::string &other_type) const
{
    return other_type == type;
}
