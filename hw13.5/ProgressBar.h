#pragma once
#include <iostream>

#include "UIElement.h"

// child some external class 
class ExternalProgressBar
{
public:
    void DrawBar()
    {
        std::cout << "ExternalProgressBar UIElement" << std::endl;
    }
};

class ProgressBar : public UIElement, ExternalProgressBar
{
public:
    void Draw() override
    {
        DrawBar();
    }
};
