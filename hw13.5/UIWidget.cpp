#include "UIWidget.h"

#include "Button.h"
#include "ProgressBar.h"
#include "Text.h"

void UIWidget::Build()
{
    // Типа сложная логика
    container.Add(new Button());
    container.Add(new Text());
    container.Add(new ProgressBar());
}

void UIWidget::Draw()
{
    container.Draw();
}
