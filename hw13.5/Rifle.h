#pragma once
#include "Weapon.h"

class Rifle : public Weapon
{
public:
    ~Rifle() override;
    void Fire() override;
    void Reload() override;
};
