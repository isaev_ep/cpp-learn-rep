#pragma once
#include <string>
#include "Data.h"

class Strategy
{
public:
    virtual Data* execute(std::string &type) = 0;
};
