#pragma once
#include "State.h"

class VeteranState : public State
{
public:
    VeteranState(Unit* unit) : State(unit) { }
    
    void attack() override;
};
