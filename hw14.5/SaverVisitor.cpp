#include "SaverVisitor.h"

#include <iostream>

void SaverVisitor::visit(Mage* mage)
{
    std::cout << "Visitor on " << mage->getName() << std::endl; 
}

void SaverVisitor::visit(Warrior* warrior)
{
    std::cout << "Visitor on " << warrior->getName() << std::endl; 
}
