#include "EventManager.h"

void EventManager::subscribe(Subscriber* client)
{
    subscribers.push_back(client);
}

void EventManager::fireEvent()
{
    for (auto subscriber : subscribers)
    {
        subscriber->onEvent();
    }
}
