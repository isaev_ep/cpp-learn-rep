#include "Button.h"

Button::Button(Command* command): command(command) { }

Command* Button::getCommand()
{
    return command;
}
