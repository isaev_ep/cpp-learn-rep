#pragma once
#include "Mage.h"
#include "Warrior.h"

class Visitor
{
public:
    virtual void visit(Mage* mage) = 0;
    virtual void visit(Warrior* warrior) = 0;
};
