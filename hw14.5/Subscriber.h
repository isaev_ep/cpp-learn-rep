#pragma once

class Subscriber
{
public:
    virtual void onEvent() = 0;
};
