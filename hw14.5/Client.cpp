#include "Client.h"

#include <iostream>

void Client::onEvent()
{
    std::cout << "Event fired on " << name << std::endl; 
}
