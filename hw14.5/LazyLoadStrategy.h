#pragma once
#include <vector>
#include "Strategy.h"

class LazyLoadStrategy : public Strategy
{
    std::vector<Data*> loaded_data;
    
public:
    Data* execute(std::string& type) override;
};
