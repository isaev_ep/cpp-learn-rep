#pragma once
class Unit;

class State
{
    Unit* unit = nullptr;
public:
    State(Unit* unit) : unit(unit) { }

    virtual void attack() = 0;
};
