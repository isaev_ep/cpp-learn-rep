#include "DataBase.h"

#include <iostream>

void DataBase::setStrategy(Strategy* new_strategy)
{
    strategy = new_strategy;
}

Data* DataBase::execute(std::string& type)
{
    return strategy->execute(type);
}
