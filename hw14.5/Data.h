#pragma once
#include <string>

class Data
{
    std::string type;

public:
    Data(std::string &type);
    bool isEqual(const std::string &other_type) const;
};
