
#include <iostream>
#include <vector>
#include <string>

#include "Button.h"
#include "Client.h"
#include "DataBase.h"
#include "DefaultLoadStrategy.h"
#include "EventManager.h"
#include "LazyLoadStrategy.h"
#include "LoadCommand.h"
#include "SaveCommand.h"
#include "SaverVisitor.h"
#include "Unit.h"

int main(int argc, char* argv[])
{
    std::setlocale(LC_ALL, "en_US.UTF-8");
    std::cout << "### Команда" << std::endl;
    std::vector<Button*> buttons {
        new Button(new SaveCommand()),
        new Button(new LoadCommand()),
    };

    for (auto button : buttons)
    {
        button->getCommand()->execute();
    }

    std::cout << "### Состояние" << std::endl;
    auto unit = new Mage("Gradable");
    unit->attack();
    unit->upgrade();
    unit->attack();

    std::cout << "### Наблюдатель" << std::endl;
    auto client1 = new Client("client1");
    auto client2 = new Client("client2");
    auto client3 = new Client("client3");
    auto event_manager = new EventManager();
    event_manager->subscribe(client1);
    event_manager->subscribe(client2);
    event_manager->subscribe(client3);
    event_manager->fireEvent();

    std::cout << "### Посетитель" << std::endl;
    auto mage = new Mage("John");
    auto warrior = new Warrior("Bob");
    auto saver = new SaverVisitor();
    mage->accept(saver);
    warrior->accept(saver);

    std::cout << "### Стратегия" << std::endl;
    auto db = new DataBase();
    auto lazy_strategy = new LazyLoadStrategy();
    auto default_strategy = new DefaultLoadStrategy();
    db->setStrategy(lazy_strategy);
    std::string finding_data_type1 = "SomeType1";
    std::string finding_data_type2 = "SomeType2";
    Data* data1 = db->execute(finding_data_type1);
    Data* data2 = db->execute(finding_data_type1);
    Data* data3 = db->execute(finding_data_type2);
    db->setStrategy(default_strategy);
    Data* data4 = db->execute(finding_data_type1);
    Data* data5 = db->execute(finding_data_type1);

    return 0;
}
