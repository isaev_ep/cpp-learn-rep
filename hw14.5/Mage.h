#pragma once
#include "Unit.h"

class Mage : public Unit
{
public:
    Mage(std::string name) : Unit(name) { }

    void accept(Visitor* visitor) override;
};
