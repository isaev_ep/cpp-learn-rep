#pragma once
#include "Visitor.h"

class SaverVisitor : public Visitor
{
public:
    void visit(Mage* mage) override;
    void visit(Warrior* warrior) override;
};
