#include "Mage.h"
#include "Visitor.h"

void Mage::accept(Visitor* visitor)
{
    visitor->visit(this);
}
