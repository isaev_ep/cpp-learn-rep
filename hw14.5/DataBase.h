#pragma once
#include <vector>
#include <string>

#include "Data.h"
#include "Strategy.h"

class DataBase
{
    Strategy* strategy;
public:
    void setStrategy(Strategy* new_strategy);
    Data* execute(std::string &type);
};
