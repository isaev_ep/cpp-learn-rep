#pragma once
#include <string>
#include <utility>

#include "Subscriber.h"

class Client : public Subscriber
{
    std::string name;
public:
    Client(std::string name) : name(std::move(name)) { }
    void onEvent() override;
};
