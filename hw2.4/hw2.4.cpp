#include <iostream>

class BarClass
{
public:
    BarClass(int x, int y)
    {
        this->x = x;
        this->y = y;

        std::cout << "Create BarClass: " << x << ", " << y << std::endl;
    }

    BarClass(const BarClass& other)
    {
        this->x = other.x;
        this->y = other.y;

        std::cout << "Copy BarClass: " << x << ", " << y << std::endl;
    }

private:
    int x;
    int y;
};

class FooClass {
public:
    FooClass(int size_X, int size_Y, std::string someText)
    {
        std::cout << "Create FooClass" << std::endl;

        this->size_X = size_X;
        this->size_Y = size_Y;

        this->someArray = new BarClass** [size_X];

        for (int x = 0; x < size_X; ++x)
        {
            this->someArray[x] = new BarClass* [size_Y];

            for (int y = 0; y < size_Y; ++y)
            {
                this->someArray[x][y] = new BarClass(x, y);
            }
        }

        this->someSting = new std::string(someText);
        someClass = new BarClass(-1, -1);
    }

    FooClass(const FooClass& other)
    {
        std::cout << "Copy FooClass" << std::endl;

        this->size_X = other.size_X;
        this->size_Y = other.size_Y;

        someArray = new BarClass** [other.size_X];

        for (int x = 0; x < size_X; ++x)
        {
            this->someArray[x] = new BarClass* [other.size_Y];

            for (int y = 0; y < size_Y; ++y)
            {
                this->someArray[x][y] = new BarClass(*(other.someArray[x][y]));
            }
        }

        someSting = new std::string(*(other.someSting));
        someClass = new BarClass(*(other.someClass));
    }

    ~FooClass()
    {
        std::cout << "Destruct FooClass" << std::endl;

        for (int x = 0; x < size_X; ++x)
        {
            for (int y = 0; y < size_Y; ++y)
            {
                delete this->someArray[x][y];
            }

            delete [] this->someArray[x];
        }

        delete [] this->someArray;
        delete this->someClass;
        delete this->someSting;
    }

    std::string* GetString();
private:
    int size_X;
    int size_Y;

    BarClass*** someArray;
    std::string* someSting;
    BarClass* someClass;
};

std::string* FooClass::GetString()
{
    return someSting;
}

int main()
{
    FooClass a(3, 3, "some text");
    FooClass* b = new FooClass(a);
    FooClass c(*(b));
    FooClass d(c);

    delete b;

    std::cout << *(d.GetString()) << std::endl;

    return 0;
}
